Creates Features templates for you to easily create features with. This is especially useful if you're a website factory with a custom install profile. This module also assumes you use Features as a deployment tool, not to make actual, reusable features like "Blog".

Steps
----

1. Set up your wysiwyg editor or production settings or other configuration.
2. Create a template like you would a feature: select components and their objects.
3. Put it in the install profile (it's a hook!).
4. Have a website with the install profile.
5. Create a new feature from the template: no work AND you won't forget that one tiny variable you always forget.

If you're a solo or occasional websiter, don't bother. It's useful if you have a lot of websites sharing something (like an install profile).
